FROM python:3.7.3-alpine3.9

COPY my-app.py /my-app.py

ENTRYPOINT [ "/usr/local/bin/python3" ]
CMD [ "/my-app.py" ]
